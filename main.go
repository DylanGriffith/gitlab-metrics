package main

import "os"
import "fmt"
import "io/ioutil"
import "net/http"
import "encoding/json"
import "encoding/csv"

type MergeRequestWithIdOnly struct {
	Iid int
}

type MergeRequest struct {
	WebUrl    string `json:"web_url"`
	MergedAt  string `json:"merged_at"`
	CreatedAt string `json:"created_at"`
	Title     string `json:"title"`
}

func get(path string) []byte {
	client := &http.Client{}

	req, err := http.NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/%s", path), nil)

	if err != nil {
		fmt.Println(err)
		os.Exit(3)
	}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		os.Exit(3)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(3)
	}

	return body
}

func main() {

	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s <label>\n", os.Args[0])
		os.Exit(3)
	}

	var label = os.Args[1]

	body := get(fmt.Sprintf("projects/gitlab-org%%2Fgitlab-ce/merge_requests?labels=%s&state=merged&per_page=200", label))
	var ids = make([]MergeRequestWithIdOnly, 0)

	err := json.Unmarshal(body, &ids)

	if err != nil {
		fmt.Println(os.Stderr, err)
		os.Exit(3)
	}

	w := csv.NewWriter(os.Stdout)
	defer w.Flush()

	w.Write([]string{"Title", "Merged At", "Web URL", "Created At"})

	for _, id := range ids {
		body = get(fmt.Sprintf("projects/gitlab-org%%2Fgitlab-ce/merge_requests/%d", id.Iid))

		var mr MergeRequest

		err := json.Unmarshal(body, &mr)

		if err != nil {
			fmt.Println(os.Stderr, err)
			os.Exit(3)
		}

		w.Write([]string{mr.Title, mr.MergedAt, mr.WebUrl, mr.CreatedAt})
		w.Flush()
	}
}
