# GitLab Metrics

Script to pull merged MRs and output as CSV

## USAGE

Fetch the last 200 merged MRs for `Monitoring`:

```bash
GITLAB_API_TOKEN=my-token go run main.go Monitoring
```

Fetch last 200 merged MRs for `CI/CD` and write to CSV:

```bash
GITLAB_API_TOKEN=my-token go run main.go CI%2FCD > cicd_merged.csv
```
